﻿using System;
using System.Linq;
using System.Web.Mvc;
using CodeFirst.Models;
using CodeFirst.Models.ViewModels;

namespace CodeFirst.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var db = new BlogsDbContext();            
            var model = db.BlogPosts.ToList();
            return View(model);                            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Create(AuthorViewModel author)
        {
            if (!ModelState.IsValid)
            {
                
            }
            
        }
    }
}