﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CodeFirst.Models.Configurations;

namespace CodeFirst.Models
{
    public class BlogsDbContext : DbContext
    {
        public BlogsDbContext()   
            :base("DefaultConnection")
        {            
        }

        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<Author> Authors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new AuthorConfigurations());
        }
    }
}