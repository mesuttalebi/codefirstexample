namespace CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuthorConfig : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BlogPosts", "Author_Id", "dbo.Authors");
            RenameColumn(table: "dbo.BlogPosts", name: "Author_Id", newName: "Author_MyId");
            RenameIndex(table: "dbo.BlogPosts", name: "IX_Author_Id", newName: "IX_Author_MyId");
            DropPrimaryKey("dbo.Authors");
            DropColumn("dbo.Authors", "Id");
            AddColumn("dbo.Authors", "MyId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Authors", "Name", c => c.String(nullable: false, maxLength: 200));
            AddPrimaryKey("dbo.Authors", "MyId");
            AddForeignKey("dbo.BlogPosts", "Author_MyId", "dbo.Authors", "MyId");            
        }
        
        public override void Down()
        {
            AddColumn("dbo.Authors", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.BlogPosts", "Author_MyId", "dbo.Authors");
            DropPrimaryKey("dbo.Authors");
            AlterColumn("dbo.Authors", "Name", c => c.String());
            DropColumn("dbo.Authors", "MyId");
            AddPrimaryKey("dbo.Authors", "Id");
            RenameIndex(table: "dbo.BlogPosts", name: "IX_Author_MyId", newName: "IX_Author_Id");
            RenameColumn(table: "dbo.BlogPosts", name: "Author_MyId", newName: "Author_Id");
            AddForeignKey("dbo.BlogPosts", "Author_Id", "dbo.Authors", "Id");
        }
    }
}
