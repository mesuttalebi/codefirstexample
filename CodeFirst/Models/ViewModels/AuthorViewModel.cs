﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodeFirst.Models.ViewModels
{
    public class AuthorViewModel 
    {
        [Display(Name="Author No")]
        [Required(ErrorMessage = "{0} Gerekli!")]
        public int MyId { get; set; }

        [Display(Name = "Author Name")]
        [Required(ErrorMessage = "{0} Gerekli!")]
        [StringLength(200, ErrorMessage = "{0} en fazla {1} karakter olabilir!")]
        [DataType(DataType.MultilineText)]
        public string Name { get; set; }

        [Display(Name="Blog yazıları")]
        public virtual ICollection<BlogPost> BlogPosts { get; set; }
       
    }
}