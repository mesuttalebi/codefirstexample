using System.Collections.Generic;
using CodeFirst.Models;

namespace CodeFirst.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CodeFirst.Models.BlogsDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CodeFirst.Models.BlogsDbContext";
        }

        protected override void Seed(CodeFirst.Models.BlogsDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Authors.AddOrUpdate(author=> author.Name, new Author[]
            {
                new Author()
                {
                    Name = "Ayt�l",
                    BlogPosts = new List<BlogPost>()
                    {
                        new BlogPost() { Title = "Blog Post 1", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now.AddMonths(-4)},
                        new BlogPost() { Title = "Blog Post 2", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now.AddMonths(-4)},
                        new BlogPost() { Title = "Blog Post 3", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now.AddMonths(-4)}
                    }
                },
                 new Author()
                {
                    Name = "Aziz",
                    BlogPosts = new List<BlogPost>()
                    {
                        new BlogPost() { Title = "Aziz Blog Post 4", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now},
                        new BlogPost() { Title = "Aziz Blog Post 5", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now},
                        new BlogPost() { Title = "Aziz Blog Post 6", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now}
                    }
                },
                 new Author()
                {
                    Name = "Mesut",
                    BlogPosts = new List<BlogPost>()
                    {
                        new BlogPost() { Title = "Mesut Blog Post 1", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now.AddMonths(-1)},
                        new BlogPost() { Title = "Mesut Blog Post 2", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now.AddMonths(-1)},
                        new BlogPost() { Title = "Mesut Blog Post 3", Content = "adsfkjhsdkjhkjhf", Date = DateTime.Now.AddMonths(-1)}
                    }
                }
            });

        }
    }
}
